package com.zxj.jpa;

import com.zxj.wrapper.AbstractWrapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * @Description:
 * @Author: zhangxuejie
 * @Date 2023-4-24 15:51
 * @Version v1.0
 */
@NoRepositoryBean
public interface BaseRepository<T, ID extends Serializable> extends JpaRepository<T, ID>, JpaSpecificationExecutor<T> {

    /**
     * 条件查询
     * @param wrapper 条件构造器
     * @return 符合条件的结果集
     */
    List<T> findAll(AbstractWrapper wrapper);

    /**
     * 分页条件查询
     * @param wrapper 条件构造器
     * @param pageable 分页对象
     * @return
     */
    Page<T> findAll(AbstractWrapper wrapper, Pageable pageable);

    /**
     * 查询单条结果
     * @param wrapper 条件构造器
     * @return Optional<Entity>
     */
    Optional<T> findOne(AbstractWrapper wrapper);
}
