package com.zxj.jpa;

import org.hibernate.query.criteria.internal.CriteriaBuilderImpl;
import org.hibernate.query.criteria.internal.compile.RenderingContext;
import org.hibernate.query.criteria.internal.expression.LiteralExpression;

import javax.persistence.criteria.CriteriaBuilder;

/**
 * 继承LiteralExpression类重写 render 方法直接返回字面量,
 * @author zhangxuejie
 * @date 2023/08/21
 */
public class HibernateInlineExpression extends LiteralExpression<String> {
    private static final long serialVersionUID = 8864997526090186676L;

    public HibernateInlineExpression(CriteriaBuilder criteriaBuilder, String literal) {
        super((CriteriaBuilderImpl) criteriaBuilder, literal);
    }

    @Override
    public String render(RenderingContext renderingContext) {
        return getLiteral();
    }
}
