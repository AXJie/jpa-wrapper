package com.zxj;

import com.zxj.jpa.BaseRepositoryFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(repositoryFactoryBeanClass = BaseRepositoryFactory.class)
public class JpaWrapperApplication {

    public static void main(String[] args) {
        SpringApplication.run(JpaWrapperApplication.class, args);
    }

}
