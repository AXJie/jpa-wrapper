package com.zxj.wrapper;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * Wrapper 操作解析
 * @author zhangxuejie
 * @date 2023/08/21
 */
@FunctionalInterface
public interface GetOperation {

    /**
     * 将TempPredicate解析为Predicate
     * @param criteriaBuilder jpa条件构造器
     * @param root 根实体对象
     * @param predicate 临时谓词
     * @return Predicate
     */
    Predicate toPredicate(CriteriaBuilder criteriaBuilder, Root<?> root, TempPredicate predicate);
}
