package com.zxj.wrapper;

import com.zxj.jpa.HibernateInlineExpression;
import com.zxj.wrapper.enums.Operator;
import org.apache.commons.lang3.time.DateUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Jpa条件构造操作工厂
 * 统一维护各种查询操作,将临时谓词转为Jpa所需的谓词,并执行一些附加操作
 * @author zhangxuejie
 * @date 2023/08/21
 */
public class OperationFactory<T> {
    private static final Map<Operator, GetOperation> GET_OPERATION_MAP = new HashMap<>();
    private static final String ORACLE = "Oracle";
    private static final String[] PARSE_PATTERNS = {
            "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM",
            "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm", "yyyy/MM",
            "yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm", "yyyy.MM"};

    static {
        GET_OPERATION_MAP.put(Operator.EQ, (cb, root, pred) -> cb.equal(root.get(pred.getFieldName()), pred.getValue()));
        GET_OPERATION_MAP.put(Operator.NE, (cb, root, pred) -> cb.notEqual(root.get(pred.getFieldName()), pred.getValue()));
        GET_OPERATION_MAP.put(Operator.GE, (cb, root, pred) -> cb.ge(root.get(pred.getFieldName()), (Number) pred.getValue()));
        GET_OPERATION_MAP.put(Operator.GT, (cb, root, pred) -> cb.gt(root.get(pred.getFieldName()), (Number) pred.getValue()));
        GET_OPERATION_MAP.put(Operator.LE, (cb, root, pred) -> cb.le(root.get(pred.getFieldName()), (Number) pred.getValue()));
        GET_OPERATION_MAP.put(Operator.LT, (cb, root, pred) -> cb.lt(root.get(pred.getFieldName()), (Number) pred.getValue()));
        GET_OPERATION_MAP.put(Operator.IN, (cb, root, pred) -> {
            CriteriaBuilder.In<Object> in = cb.in(root.get(pred.getFieldName()));
            Object[] values = (Object[]) pred.getValue();
            for (Object value : values) {
                in.value(value);
            }
            return in;
        });
        GET_OPERATION_MAP.put(Operator.NOTIN, (cb, root, pred) -> {
            CriteriaBuilder.In<Object> in = cb.in(root.get(pred.getFieldName()));
            Object[] values = (Object[]) pred.getValue();
            for (Object value : values) {
                in.value(value);
            }
            return cb.not(in);
        });
        GET_OPERATION_MAP.put(Operator.LIKE, (cb, root, pred) -> cb.like(root.get(pred.getFieldName()), String.valueOf(pred.getValue())));
        GET_OPERATION_MAP.put(Operator.ISNULL, (cb, root, pred) -> cb.isNull(root.get(pred.getFieldName())));
        GET_OPERATION_MAP.put(Operator.NOTNULL, (cb, root, pred) -> cb.isNotNull(root.get(pred.getFieldName())));
        GET_OPERATION_MAP.put(Operator.BETWEEN, (cb, root, pred) -> {
            Class<?> javaType = root.get(pred.getFieldName()).getJavaType();
            Object[] objects = (Object[]) pred.getValue();
            Object lower = objects[0];
            Object upper = objects[1];
            if (lower instanceof String) {
                String lower1 = (String) lower;
                String upper1 = (String) upper;
                // 如果实体中构建条件的熟悉类型为Date类型则将字符串转换为时间类型
                if (javaType == Date.class) {
                    try {
                        return cb.between(root.get(pred.getFieldName()),
                                DateUtils.parseDate(lower1, PARSE_PATTERNS), DateUtils.parseDate(upper1, PARSE_PATTERNS));
                    } catch (ParseException e) {
                        throw new IllegalArgumentException("传入参数非时间格式 无法进行转换 导致与查询条件预期类型不匹配");
                    }
                } else {
                    return cb.between(root.get(pred.getFieldName()), lower1, upper1);
                }
            } else if (lower instanceof LocalDateTime) {
                return cb.between(root.get(pred.getFieldName()), (LocalDateTime) lower, (LocalDateTime) upper);
            } else if (lower instanceof Date) {
                return cb.between(root.get(pred.getFieldName()), (Date) lower, (Date) upper);
            } else if (Number.class.isAssignableFrom(lower.getClass())) {
                return cb.between(root.get(pred.getFieldName()), (Comparable)lower, (Comparable)upper);
            } else {
                throw new RuntimeException("between只能用于 number str date");
            }
        });
        GET_OPERATION_MAP.put(Operator.LIKE_IGNORE_SPACES, (cb, root, pred) -> cb.like(cb.function("REPLACE", String.class,
                root.get(pred.getFieldName()), cb.literal(" "), cb.literal("")), String.valueOf(pred.getValue())));
        GET_OPERATION_MAP.put(Operator.EXT_EQ, (cb, root, pred) -> cb.equal(getExtExpression(root, cb, pred), pred.getValue()));
        GET_OPERATION_MAP.put(Operator.EXT_LIKE, (cb, root, pred) -> cb.like(getExtExpression(root, cb, pred), String.valueOf(pred.getValue())));
    }

    public static Predicate getOperation(CriteriaBuilder cb, Root<?> root, TempPredicate predicate) {
        return GET_OPERATION_MAP.get(predicate.getOperator()).toPredicate(cb, root, predicate);
    }

    /**
     * 通过正则表达式实现扩展列的条件查询
     *
     * @param criteriaBuilder 条件构造器
     * @param root 根实体对象
     * @param predicate 临时断言/谓词
     * @return
     */
    private static Expression<String> getExtExpression(Root<?> root, CriteriaBuilder criteriaBuilder, TempPredicate predicate) {
        String functionName;
        String[] split = predicate.getFieldName().split("\\.");
        String exlColumn = split[0];
        String column = split[1];
        if (DatabaseVersionChecker.getDatabaseVersionFull().contains(ORACLE)) {
            // oracle版本是否大于等于12.2.0.1.0 在该版本Oracle引入了 JSON_VALUE 函数可以实现JSON值的条件查询
            if (DatabaseVersionChecker.getCurrentVersionGt122010()) {
                functionName = "JSON_VALUE";
            } else {
                return criteriaBuilder.function("REGEXP_SUBSTR", String.class,
                        root.get(split[0]),
                        criteriaBuilder.literal("\"" + column + "\"\\s*:\\s*\"([^\"]+)\""),
                        criteriaBuilder.literal(1),
                        criteriaBuilder.literal(1),
                        criteriaBuilder.literal('i'),
                        criteriaBuilder.literal(1));
            }
        } else {
            functionName = "JSON_EXTRACT";
        }
        return criteriaBuilder.function(functionName, String.class, root.get(exlColumn),
                new HibernateInlineExpression(criteriaBuilder, "'$." + column + "'"));
    }
}