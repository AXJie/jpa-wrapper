package com.zxj.wrapper;

import java.lang.invoke.SerializedLambda;

/**
 * @Description: lambda版jpa条件构造器
 * @Author: zhangxuejie
 * @Date 2023-3-19 18:39
 * @Version v1.0
 */
public class LambdaQueryWrapper<T> extends AbstractWrapper<T, SFunction<T, ?>, LambdaQueryWrapper<T>> {
    public LambdaQueryWrapper() {

    }

    /**
     * OR
     *
     * @param tempQueryWrapper
     * @return this
     */
    public LambdaQueryWrapper<T> or(Boolean condition,TempQueryWrapper<T> tempQueryWrapper) {
        if(condition){
            LambdaQueryWrapper<T> queryWrapper = new LambdaQueryWrapper<>();
            tempQueryWrapper.exec(queryWrapper);
            if (!queryWrapper.tempPredicates.isEmpty()) {
                this.orTempPredicates.add(queryWrapper.tempPredicates);
            }
        }
        return this;
    }

    /**
     * OR
     *
     * @param tempQueryWrapper
     * @return this
     */
    public LambdaQueryWrapper<T> or(TempQueryWrapper<T> tempQueryWrapper) {
        return or(true,tempQueryWrapper);
    }

    /**
     * 用于or的情况
     */
    public interface TempQueryWrapper<T> {
        /**
         * 构建复杂条件
         *
         * @param wrapper
         */
        void exec(LambdaQueryWrapper<T> wrapper);
    }

    /**
     * 查询指定字段
     *
     * @param fieldNames 查询字段
     * @return 当前对象
     * @SafeVarargs注解 告诉编译器开发者已经确保使用泛型可变参数时不会导致堆污染问题
     */
    @SafeVarargs
    public final LambdaQueryWrapper<T> select(SFunction<T, ?>... fieldNames) {
        if (fieldNames != null && fieldNames.length > 0) {
            setSelectFields(fieldNames);
        }
        return this;
    }

    @SafeVarargs
    private final void setSelectFields(SFunction<T, ?>... fieldNames) {
        for (SFunction<T, ?> fieldName : fieldNames) {
            SerializedLambda resolve = JpaQueryWrapperUtil.resolve((SFunction<?, ?>) fieldName);
            this.selectColumn.add(JpaQueryWrapperUtil.getFieldName(resolve.getImplMethodName()));
        }
    }
}
