package com.zxj.wrapper;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;

/**
 * @Description: jpa条件构造器
 * @Author: zhangxuejie
 * @Date 2023-3-16 19:29
 * @Version v1.0
 */
@Slf4j
public class QueryWrapper<T> extends AbstractWrapper<T, String, QueryWrapper<T>> {

    public QueryWrapper() {

    }

    /**
     * OR
     *
     * @param condition
     * @param tempQueryWrapper
     * @return this
     */
    public QueryWrapper<T> or(Boolean condition, TempQueryWrapper<T> tempQueryWrapper) {
        if (condition) {
            QueryWrapper<T> queryWrapper = new QueryWrapper<>();
            tempQueryWrapper.exec(queryWrapper);
            if (!queryWrapper.tempPredicates.isEmpty()) {
                this.orTempPredicates.add(queryWrapper.tempPredicates);
            }
        }
        return this;
    }

    /**
     * OR
     *
     * @param tempQueryWrapper
     * @return this
     */
    public QueryWrapper<T> or(TempQueryWrapper<T> tempQueryWrapper) {
        return or(true, tempQueryWrapper);
    }

    /**
     * 用于or的情况
     */
    public interface TempQueryWrapper<T> {
        /**
         * 构建复杂条件
         *
         * @param wrapper
         */
        void exec(QueryWrapper<T> wrapper);
    }

    /**
     * 查询指定字段
     *
     * @param fieldNames 查询字段
     * @return 当前对象
     */
    public QueryWrapper<T> select(String... fieldNames) {
        this.selectColumn.addAll(Arrays.asList(fieldNames));
        return this;
    }
}
