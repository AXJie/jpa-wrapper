package com.zxj.wrapper;

import java.lang.invoke.SerializedLambda;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 存放JpaQueryWrapper所使用到的工具类
 * @Author: zhangxuejie
 * @Date 2023-3-18 20:18
 * @Version v1.0
 */
public class JpaQueryWrapperUtil {
    /**
     * 通过反序列化解析lambda表达式,该方法只能解析lambda写法的对象
     * 当函数式接口实现了Serializable接口后,里边会有个私有的 writeReplace 方法
     * 返回SerializedLambda(lambda表达式序列化形式)
     *
     * @param lambda
     * @param <T>
     * @return 序列化形式的lambda表达式(存放着一些元数据信息)
     */
    public static <T> SerializedLambda resolve(SFunction<T, ?> lambda) {
        Method writeReplaceMethod;
        try {
            writeReplaceMethod = lambda.getClass().getDeclaredMethod("writeReplace");
        } catch (NoSuchMethodException e) {
            throw new RuntimeException("该方法只能传入函数式接口使用lambda表达式实现,产生的函数式接口实现类");
        }
        // 从序列化方法取出序列化的lambda信息
        writeReplaceMethod.setAccessible(true);
        SerializedLambda serializedLambda;
        try {
            serializedLambda = (SerializedLambda) writeReplaceMethod.invoke(lambda);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }
        return serializedLambda;
    }

    /**
     * 解析方法名,获取属性名称
     * get方法开头为 is 或者 get，将方法名 去除is或者get，然后首字母小写，就是属性名
     *
     * @param methodName 方法名
     * @return 属性名
     */
    public static String getFieldName(String methodName) {
        int prefixLen = methodName.startsWith("is") ? 2 : 3;
        return (String.valueOf(methodName.charAt(prefixLen))).toLowerCase()
                + methodName.substring(prefixLen + 1);
    }
}
