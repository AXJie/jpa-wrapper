package com.zxj.wrapper;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Description: 服务启动时获取连接数据库版本号
 * @Author: zhangxuejie
 * @Date 2023-4-10 22:25
 * @Version v1.0
 */
@Slf4j
@Component
public class DatabaseVersionChecker {
    @Autowired
    private DataSource dataSource;
    /**
     * 连接数据库版本号
     */
    private static String databaseVersion;

    /**
     * 是否大于或等于12.2.0.1.0,
     * oracle在该版本引入了JSON_VALUE函数,作用与扩展字段查询，效率比正则表达式写法更高
     */
    private static Boolean currentVersionGt122010;
    /**
     * 通过预编译提高匹配速度
     */
    private static final Pattern VERSION_PATTERN = Pattern.compile("(\\d+\\.\\d+\\.\\d+\\.\\d+)");

    @PostConstruct
    public void init() {
        try (Connection connection = dataSource.getConnection()) {
            DatabaseMetaData metaData = connection.getMetaData();
            databaseVersion = metaData.getDatabaseProductVersion();
            String currentVersion = getDatabaseVersion();
            if (currentVersion != null) {
                currentVersionGt122010 = compareVersion(currentVersion, "12.2.0.1.0");
                log.info("Database version: {}", getDatabaseVersionFull());
            } else {
                log.error("Failed to extract database version.");
            }
        } catch (SQLException e) {
            log.error("Error connecting to database: {}", e.getMessage());
        }
    }

    /**
     * 获取连接数据库具体版本号信息
     *
     * @return
     */
    public static String getDatabaseVersion() {
        // 通过正则表达式截取具体的版本号信息 如何 12.2.0.1.0
        Matcher matcher = VERSION_PATTERN.matcher(databaseVersion);
        return matcher.find() ? matcher.group(1) : null;
    }

    /**
     * 获取连接数据库版本全信息
     *
     * @return
     */
    public static String getDatabaseVersionFull() {
        return databaseVersion;
    }

    public static Boolean getCurrentVersionGt122010() {
        return currentVersionGt122010;
    }

    /**
     * 比较两个版本号，v1大于或等于v2返回true
     *
     * @param v1
     * @param v2
     */
    public Boolean compareVersion(String v1, String v2) {
        String[] version1Parts = v1.split("\\.");
        String[] version2Parts = v2.split("\\.");

        int length = Math.min(version1Parts.length, version2Parts.length);
        for (int i = 0; i < length; i++) {
            int version1 = Integer.parseInt(version1Parts[i]);
            int version2 = Integer.parseInt(version2Parts[i]);
            if (version1 < version2) {
                return false;
            } else if (version1 > version2) {
                return true;
            }
        }
        return true;
    }
}
