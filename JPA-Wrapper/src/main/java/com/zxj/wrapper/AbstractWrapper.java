package com.zxj.wrapper;

import com.zxj.wrapper.enums.Operator;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.Assert;

import javax.persistence.criteria.*;
import java.lang.invoke.SerializedLambda;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.StreamSupport;

import static com.zxj.wrapper.OperationFactory.getOperation;


/**
 * @Description: 条件构造器抽象类 T 实体泛型,R:参数类型(String、SFunction),Children:继承当前抽象类的子类
 * @Author: zhangxuejie
 * @Date 2023-3-18 22:53
 * @Version v2.0
 */
@Slf4j
public abstract class AbstractWrapper<T, R, Children extends AbstractWrapper<T, R, Children>> {
    protected final Children typedThis = (Children) this;
    /**
     * 过滤条件
     */
    protected final List<TempPredicate> tempPredicates = new ArrayList<>();
    /**
     * 排序
     */
    protected final List<TempOrder> TempOrderList = new ArrayList<>();
    /**
     * 过滤条件-复杂的or的时候用的
     */
    protected final List<List<TempPredicate>> orTempPredicates = new ArrayList<>();
    /**
     * 查询字段
     */
    protected final List<String> selectColumn = new ArrayList<>();


    public AbstractWrapper() {
    }

    /**
     * 转换为JPA用的Specification对象
     *
     * @return
     */
    public Specification<T> build() {
        return (root, query, cb) -> {
            Predicate[] predicates = new Predicate[tempPredicates.size() + orTempPredicates.size()];
            int index = 0;
            // 过滤条件
            for (int i = 0; i < tempPredicates.size(); ++i) {
                predicates[i] = getOperation(cb, root, tempPredicates.get(i));
                index++;
            }
            // or的过滤条件
            for (List<TempPredicate> orTempPredicate : orTempPredicates) {
                Predicate[] oneOr = new Predicate[orTempPredicate.size()];
                for (int i = 0; i < orTempPredicate.size(); ++i) {
                    oneOr[i] = getOperation(cb, root, tempPredicates.get(i));
                }
                predicates[index] = cb.or(oneOr);
                index++;
            }

            if (predicates.length == 0 && selectColumn.isEmpty() && TempOrderList.isEmpty()) {
                // 如果 检索条件(predicates)、检索列(selectColumn)、排序(TempOrderList)都为空时 则直接返回null
                return null;
            } else if (TempOrderList.isEmpty() && selectColumn.isEmpty()) {
                return cb.and(predicates);
            } else if (selectColumn.isEmpty() && predicates.length != 0) {
                Order[] orders = getOrders(root, cb);
                return query.orderBy(orders).where(predicates).getRestriction();
            } else if (TempOrderList.isEmpty() && predicates.length != 0) {
                Selection<?>[] selections = this.selectColumn.stream().map(root::get).toArray(Selection<?>[]::new);
                return query.multiselect(selections).where(predicates).getRestriction();
            } else if (selectColumn.isEmpty()) {
                Order[] orders = getOrders(root, cb);
                return query.orderBy(orders).getRestriction();
            } else if (TempOrderList.isEmpty()) {
                Selection<?>[] selections = this.selectColumn.stream().map(root::get).toArray(Selection<?>[]::new);
                return query.multiselect(selections).getRestriction();
            }else {
                Order[] orders = getOrders(root, cb);
                Selection<?>[] selections = this.selectColumn.stream().map(root::get).toArray(Selection<?>[]::new);
                return query.multiselect(selections)
                        .orderBy(orders).where(predicates).getRestriction();
            }
        };
    }

    /**
     * 将TempOrder 解析为 Order
     *
     * @param root 根实体对象
     * @param cb   jpa条件构造器
     * @return Order
     */
    private Order[] getOrders(Root<T> root, CriteriaBuilder cb) {
        Order[] orders = new Order[TempOrderList.size()];
        for (int i = 0; i < TempOrderList.size(); i++) {
            orders[i] = TempOrderList.get(i).isDesc()
                    ? cb.desc(root.get(TempOrderList.get(i).getProperty()))
                    : cb.asc(root.get(TempOrderList.get(i).getProperty()));
        }
        return orders;
    }

    /**
     * =
     *
     * @param condition 是否需要使用本条件
     * @param fieldName 字段名
     * @param value     值
     * @return this
     */
    public Children eq(boolean condition, R fieldName, Object value) {
        if (condition) {
            tempPredicates.add(TempPredicate.builder().fieldName(getField(fieldName))
                    .operator(Operator.EQ).value(value).build());
        }
        return typedThis;
    }

    /**
     * 通过扩展字段下的属性条件筛选 =
     *
     * @param condition 是否需要使用本条件
     * @param fieldName 字段名
     * @param value     值
     * @return this
     */
    public Children extEq(boolean condition, String fieldName, Object value) {
        if (condition) {
            tempPredicates.add(TempPredicate.builder().fieldName(fieldName)
                    .operator(Operator.EXT_EQ).value(value).build());
        }
        return typedThis;
    }

    /**
     * 扩展字段下的属性条件筛选 =
     *
     * @param fieldName 字段名
     * @param value     值
     * @return this
     */
    public Children extEq(String fieldName, Object value) {
        extEq(true, fieldName, value);
        return typedThis;
    }

    /**
     * 通过扩展字段下的属性条件筛选 like
     *
     * @param condition 是否需要使用本条件
     * @param fieldName 字段名
     * @param value     值
     * @return this
     */
    public Children extLike(boolean condition, String fieldName, Object value) {
        if (condition) {
            tempPredicates.add(TempPredicate.builder().fieldName(fieldName)
                    .operator(Operator.EXT_LIKE).value("%" + value + "%").build());
        }
        return typedThis;
    }

    /**
     * 扩展字段下的属性条件筛选 like
     *
     * @param fieldName 字段名
     * @param value     值
     * @return this
     */
    public Children extLike(String fieldName, Object value) {
        extLike(true, fieldName, value);
        return typedThis;
    }

    /**
     * 通过扩展字段下的属性条件筛选 like
     *
     * @param condition 是否需要使用本条件
     * @param fieldName 字段名
     * @param value     值
     * @return this
     */
    public Children extLikeRight(boolean condition, String fieldName, Object value) {
        if (condition) {
            tempPredicates.add(TempPredicate.builder().fieldName(fieldName)
                    .operator(Operator.EXT_LIKE).value(value + "%").build());
        }
        return typedThis;
    }

    /**
     * 扩展字段下的属性条件筛选 like
     *
     * @param fieldName 字段名
     * @param value     值
     * @return this
     */
    public Children extLikeRight(String fieldName, Object value) {
        extLikeRight(true, fieldName, value);
        return typedThis;
    }

    /**
     * =
     *
     * @param fieldName 字段名
     * @param value     值
     * @return this
     */
    public Children eq(R fieldName, Object value) {
        eq(true, fieldName, value);
        return typedThis;
    }

    /**
     * !=
     *
     * @param condition 是否需要使用本条件
     * @param fieldName 字段名
     * @param value     值
     * @return this
     */
    public Children ne(boolean condition, R fieldName, Object value) {
        if (condition) {
            tempPredicates.add(TempPredicate.builder().fieldName(getField(fieldName))
                    .operator(Operator.NE).value(value).build());
        }
        return typedThis;
    }

    /**
     * !=
     *
     * @param fieldName 字段名
     * @param value     值
     * @return this
     */
    public Children ne(R fieldName, Object value) {
        ne(true, fieldName, value);
        return typedThis;
    }

    /**
     * >
     *
     * @param condition 是否需要使用本条件
     * @param fieldName 字段名
     * @param value     值
     * @return this
     */
    public Children gt(boolean condition, R fieldName, Comparable<?> value) {
        if (condition) {
            tempPredicates.add(TempPredicate.builder().fieldName(getField(fieldName))
                    .operator(Operator.GT).value(value).build());
        }
        return typedThis;
    }

    /**
     * >
     *
     * @param fieldName 字段名
     * @param value     值
     * @return this
     */
    public Children gt(R fieldName, Comparable<?> value) {
        gt(true, fieldName, value);
        return typedThis;
    }

    /**
     * >=
     *
     * @param condition 是否需要使用本条件
     * @param @param    fieldName 字段名
     * @param value     值
     * @return this
     */
    public Children ge(boolean condition, R fieldName, Comparable<?> value) {
        if (condition) {
            tempPredicates.add(TempPredicate.builder().fieldName(getField(fieldName))
                    .operator(Operator.GE).value(value).build());
        }
        return typedThis;
    }

    /**
     * >=
     *
     * @param @param fieldName 字段名
     * @param value  值
     * @return this
     */
    public Children ge(R fieldName, Comparable<?> value) {
        ge(true, fieldName, value);
        return typedThis;
    }

    /**
     * <
     *
     * @param condition 是否需要使用本条件
     * @param fieldName 字段名
     * @param value     值
     * @return this
     */
    public Children lt(boolean condition, R fieldName, Comparable<?> value) {
        if (condition) {
            tempPredicates.add(TempPredicate.builder().fieldName(getField(fieldName))
                    .operator(Operator.LT).value(value).build());
        }
        return typedThis;
    }

    /**
     * <
     *
     * @param fieldName 字段名
     * @param value     值
     * @return this
     */
    public Children lt(R fieldName, Comparable<?> value) {
        lt(true, fieldName, value);
        return typedThis;
    }

    /**
     * <=
     *
     * @param condition 是否需要使用本条件
     * @param fieldName 字段名
     * @param value     值
     * @return this
     */
    public Children le(boolean condition, R fieldName, Comparable<?> value) {
        if (condition) {
            tempPredicates.add(TempPredicate.builder().fieldName(getField(fieldName))
                    .operator(Operator.LE).value(value).build());
        }
        return typedThis;
    }

    /**
     * <=
     *
     * @param fieldName 字段名
     * @param value     值
     * @return this
     */
    public Children le(R fieldName, Comparable<?> value) {
        le(true, fieldName, value);
        return typedThis;
    }

    /**
     * like '%xx%'
     *
     * @param condition 是否需要使用本条件
     * @param fieldName 字段名
     * @param value     值
     * @return this
     */
    public Children like(boolean condition, R fieldName, String value) {
        if (condition) {
            tempPredicates.add(TempPredicate.builder().fieldName(getField(fieldName))
                    .operator(Operator.LIKE).value("%" + value + "%").build());
        }
        return typedThis;
    }

    /**
     * like '%xx%'
     *
     * @param fieldName 字段名
     * @param value     值
     * @return this
     */
    public Children like(R fieldName, String value) {
        like(true, fieldName, value);
        return typedThis;
    }

    /**
     * like 'xx%'
     *
     * @param condition 是否需要使用本条件
     * @param fieldName 字段名
     * @param value     值
     * @return this
     */
    public Children likeRight(boolean condition, R fieldName, String value) {
        if (condition) {
            tempPredicates.add(TempPredicate.builder().fieldName(getField(fieldName))
                    .operator(Operator.LIKE).value(value + "%").build());
        }
        return typedThis;
    }

    /**
     * like 'xx%'
     *
     * @param fieldName 字段名
     * @param value     值
     * @return this
     */
    public Children likeRight(R fieldName, String value) {
        like(true, fieldName, value);
        return typedThis;
    }

    /**
     * 模糊查询(忽略空格)
     *
     * @param condition 是否需要使用本条件
     * @param fieldName 字段名
     * @param value     值
     * @return this
     */
    public Children likeIgnoreSpaces(boolean condition, R fieldName, String value) {
        if (condition) {
            tempPredicates.add(TempPredicate.builder().fieldName(getField(fieldName))
                    .operator(Operator.LIKE_IGNORE_SPACES).value(value.replace(" ", "") + "%").build());
        }
        return typedThis;
    }

    /**
     * 模糊查询(忽略空格)
     *
     * @param fieldName 字段名
     * @param value     值
     * @return this
     */
    public Children likeIgnoreSpaces(R fieldName, String value) {
        likeIgnoreSpaces(true, fieldName, value);
        return typedThis;
    }


    /**
     * between lower，upper
     *
     * @param condition 是否使用本条件
     * @param fieldName 字段名
     * @param lower     最小值
     * @param upper     最大值
     * @return
     */
    public Children between(boolean condition, R fieldName, Object lower, Object upper) {
        Assert.isTrue(lower.getClass() == upper.getClass(), "最大值与最小值必须为同类型");
        if (condition) {
            tempPredicates.add(TempPredicate.builder().fieldName(getField(fieldName))
                    .operator(Operator.BETWEEN).value(new Object[]{lower, upper}).build());
        }
        return typedThis;
    }

    /**
     * between lower，upper
     *
     * @param fieldName 字段名
     * @param lower     最小值
     * @param upper     最大值
     * @return
     */
    public Children between(R fieldName, Object lower, Object upper) {
        between(true, fieldName, lower, upper);
        return typedThis;
    }

    /**
     * in  集合
     *
     * @param condition 是否需要使用本条件
     * @param fieldName 字段名
     * @param values    值
     * @return this
     */
    public Children in(boolean condition, R fieldName, Iterable<?> values) {
        if (condition) {
            Object[] array = toArray(values);
            tempPredicates.add(TempPredicate.builder().fieldName(getField(fieldName))
                    .operator(Operator.IN).value(array).build());
        }
        return typedThis;
    }

    /**
     * in  集合
     *
     * @param fieldName 字段名
     * @param values    值
     * @return this
     */
    public Children in(R fieldName, Iterable<?> values) {
        in(true, fieldName, values);
        return typedThis;
    }

    /**
     * in  数组/可变参
     *
     * @param condition 是否需要使用本条件
     * @param fieldName 字段名
     * @param values    值
     * @return this
     */
    public Children in(boolean condition, R fieldName, Object... values) {
        if (condition) {
            tempPredicates.add(TempPredicate.builder().fieldName(getField(fieldName))
                    .operator(Operator.IN).value(values).build());
        }
        return typedThis;
    }

    /**
     * in  数组/可变参
     *
     * @param fieldName 字段名
     * @param values    值
     * @return this
     */
    public Children in(R fieldName, Object... values) {
        in(true, fieldName, values);
        return typedThis;
    }

    /**
     * notIn  数组/可变参
     *
     * @param condition 是否需要使用本条件
     * @param fieldName 字段名
     * @param values    值
     * @return this
     */
    public Children notIn(boolean condition, R fieldName, Object... values) {
        if (condition) {
            tempPredicates.add(TempPredicate.builder().fieldName(getField(fieldName))
                    .operator(Operator.NOTIN).value(values).build());
        }
        return typedThis;
    }

    /**
     * notIn  数组/可变参
     *
     * @param fieldName 字段名
     * @param values    值
     * @return this
     */
    public Children notIn(R fieldName, Object... values) {
        notIn(true, fieldName, values);
        return typedThis;
    }

    /**
     * isNull
     *
     * @param condition 是否需要使用本条件
     * @param fieldName 字段名
     * @return this
     */
    public Children isNull(boolean condition, R fieldName) {
        if (condition) {
            tempPredicates.add(TempPredicate.builder().fieldName(getField(fieldName))
                    .operator(Operator.ISNULL).build());
        }
        return typedThis;
    }

    /**
     * isNull
     *
     * @param fieldName 字段名
     * @return this
     */
    public Children isNull(R fieldName) {
        isNull(true, fieldName);
        return typedThis;
    }

    /**
     * isNotNull
     *
     * @param condition 是否需要使用本条件
     * @param fieldName 字段名
     * @return this
     */
    public Children isNotNull(boolean condition, R fieldName) {
        if (condition) {
            tempPredicates.add(TempPredicate.builder().fieldName(getField(fieldName))
                    .operator(Operator.NOTNULL).build());
        }
        return typedThis;
    }

    /**
     * isNotNull
     *
     * @param fieldName 字段名
     * @return this
     */
    public Children isNotNull(R fieldName) {
        isNotNull(true, fieldName);
        return typedThis;
    }


    /**
     * notIn  集合
     *
     * @param condition 是否需要使用本条件
     * @param fieldName 字段名
     * @param values    值
     * @return this
     */
    public Children notIn(boolean condition, R fieldName, Iterable<?> values) {
        if (condition) {
            Object[] array = toArray(values);
            tempPredicates.add(TempPredicate.builder().fieldName(getField(fieldName))
                    .operator(Operator.NOTIN).value(array).build());
        }
        return typedThis;
    }

    /**
     * notIn  集合
     *
     * @param fieldName 字段名
     * @param values    值
     * @return this
     */
    public Children notIn(R fieldName, Iterable<?> values) {
        notIn(true, fieldName, values);
        return typedThis;
    }

    /**
     * 根据字段排序 正序
     *
     * @param fieldName 字段名
     * @return
     */
    public Children TempOrderAsc(R fieldName) {
        TempOrderList.add(TempOrder.builder().isDesc(false).property(getField(fieldName)).build());
        return typedThis;
    }

    /**
     * 根据字段排序 倒序
     *
     * @param fieldName 字段名
     * @return
     */
    public Children TempOrderDesc(R fieldName) {
        TempOrderList.add(TempOrder.builder().isDesc(true).property(getField(fieldName)).build());
        return typedThis;
    }

    /**
     * 解析属性名
     *
     * @param fieldName
     * @return
     */
    private String getField(R fieldName) {
        if (fieldName instanceof String) {
            return (String) fieldName;
        } else {
            SerializedLambda resolve = JpaQueryWrapperUtil.resolve((SFunction<?, ?>) fieldName);
            return JpaQueryWrapperUtil.getFieldName(resolve.getImplMethodName());
        }
    }

    /**
     * 将迭代器转换为Object数组
     *
     * @param values 迭代器
     * @return Object数组
     */
    private static Object[] toArray(Iterable<?> values) {
        if (values != null && values.iterator().hasNext()) {
            return StreamSupport.stream(values.spliterator(), false).toArray();
        } else {
            throw new NullPointerException("传入参数数组为空！");
        }
    }
}

@Data
@Builder
class TempOrder {
    /**
     * 属性
     */
    private String property;
    /**
     * 是否是desc
     */
    private boolean isDesc;
}

@Data
@Builder
class TempPredicate {
    /**
     * 字段名
     */
    private String fieldName;
    /**
     * 操作符
     */
    private Operator operator;
    /**
     * 值
     */
    private Object value;
}
