# JPA-Wrapper
# JPA动态查询的增强封装：让开发更灵活

[![许可证](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE)

## 简介

本项目旨在为使用JPA进行动态查询构造的开发提供更加优雅便捷的解决方案。虽然**JPA Criteria API**提供了强大的工具用于构建查询，但在实际开发中，大多数需求都是关于单表的动态条件查询。而使用**JPA Criteria API**编写动态条件查询时，代码相对繁琐，不如**Mybatis Plus**中的**Wrapper**灵活。为了改善这一使用体验，我们提供了这个**JPA Wrapper**，在对**JPA Criteria API**进行二次封装和扩展的基础上，实现了类似**Mybatis Plus Wrapper**的功能。开发者能够在一定程度上实现从**Mybatis Plus**到**JPA**的无缝切换，并且可以在此基础上进行更多的扩展（如数据类型转换，通用问题处理）。

## 功能特点

- 简化动态查询构造的表达方式，更加直观明了。
- 支持常见查询操作，如等于、区间等。
- 解决共性问题，如类型转换和错误处理。
- 支持方法链式调用，代码简洁易读。
- 整合**JPA**标准查询特性。
- 提供LambdaQueryWrapper以提升代码可维护性。

## 使用示例

本项目提供了`QueryWrapper`类，用于增强**JPA Criteria API**的功能。您可以使用它来更简单地构建动态查询：

```java
// 动态条件查询
List<QaCheckTask> all = qaCheckTaskDao.findAll(new QueryWrapper<QaCheckTask>()
    .eq(taskTypeCode != null, "taskTypeCode", taskTypeCode)
    .between(checkedStartDate != null && checkedEndDate != null,
        "checkedDate", checkedStartDate, checkedEndDate));
```

## 快速入门

要在项目中使用这个库：

1. 添加依赖（包括源代码或JAR文件）。
2. 在主应用类上添加 `@EnableJpaRepositories(repositoryFactoryBeanClass = BaseRepositoryFactory.class)` 注解。
3. 使您的Repositories/Dao接口继承 `BaseRepository<您的实体类, 主键类型>`。
